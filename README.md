# parsing_auto_ria

Парсер для парсинга цен на сайте auto.ria.com в разделе новое авто.

Для парсера использовались следующие версии пакетов:
pip install beautifulsoup4==4.8.1
pip install requests==2.22.0

После запуска будут формироваться CSV файлы по каждой марке атомобиля.